__author__ = 'namax_w7'
# -*- coding: utf-8 -*-

import time

from PySide.QtWebKit import QWebView, QWebPage, QWebSettings, QWebElement, QWebElementCollection
from PySide.QtNetwork import QNetworkAccessManager, QNetworkRequest, QNetworkProxy
from PySide.QtCore import QTimer, QEventLoop, Qt, QEvent, QPoint
from PySide.QtGui import QImage, QPainter, QInputEvent, QMouseEvent


import log


class Browser(QWebView):
    def __init__(self, app, user_agent=None, proxy=None):
        QWebView.__init__(self)
        self.app = app

        manager = QNetworkAccessManager()
        # set proxy
        if proxy:
            manager.setProxy(
                QNetworkProxy(QNetworkProxy.HttpProxy,
                              proxy["host"], int(proxy["port"]),
                              proxy["login"], proxy["password"]
                              ))

        # manager.finished.connect(self.finished)
        userAgent = UserAgent()
        webpage = WebPage(user_agent or userAgent.rand_agent())
        webpage.setNetworkAccessManager(manager)
        self.setPage(webpage)
        self.delay = 5

        # signals
        self.loadFinished.connect(self._loadFinished)

        self.settings().setAttribute(QWebSettings.PluginsEnabled, True)
        self.settings().setAttribute(QWebSettings.JavaEnabled, True)
        self.settings().setAttribute(QWebSettings.AutoLoadImages, True)
        self.settings().setAttribute(QWebSettings.DeveloperExtrasEnabled, True)

        self.show()

    def _loadFinished(self, result):
        pass

    def get(self, url, timeout=15, num_retries=0):
        html = ''

        t1 = time.time()

        loop = QEventLoop()
        timer = QTimer()
        timer.setSingleShot(True)

        timer.timeout.connect(loop.quit)
        self.loadFinished.connect(loop.quit)

        request = QNetworkRequest()
        request.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
        request.setRawHeader('Accept',
                             'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8');
        request.setUrl(url);
        self.load(request)

        timer.start(timeout * 1000)
        loop.exec_()

        if timer.isActive():
            # downloaded successfully
            timer.stop()
            timer.deleteLater()
            self.stop()
        else:
            # did not download in time
            if num_retries > 0:
                log.logger.debug('Timeout - retrying')
                html = self.get(url, timeout, --num_retries)
            else:
                log.logger.debug('Timed out')
                html = ''
        timer.deleteLater()
        return html

    def wait(self, timeout=1, pattern=None):
        """Wait for delay time
        """
        deadline = time.time() + timeout
        while deadline > time.time():
            time.sleep(0)
            self.app.processEvents(QEventLoop.AllEvents)

    # def __del__(self):
    # self.setPage(None)

    def js_exec(self, js):
        self.app.processEvents()
        return self.page().mainFrame().evaluateJavaScript(js)

    def find_element(self, selector_query):
        return self.page().currentFrame().documentElement().findFirst(selector_query)

    def find_elements(self, selector_query):
        return self.page().currentFrame().documentElement().findAll(selector_query)

    def click(self, element):
        if element.isNull():
            return False

        if isinstance(element, QWebElement):
            self.scroll_and_click(element)

        return True

    def click_with_js_event(self, element):
        if element.isNull():
            return False

        if isinstance(element, QWebElement):
            self.scroll_to_element(element)
            element.evaluateJavaScript(
                "var evObj = document.createEvent('MouseEvents'); " +
                "evObj.initEvent('click', true, true); this.dispatchEvent(evObj);")

        return True

    def scroll_to_element(self, element):

        element_geometry = element.geometry()
        center = element_geometry.center()
        element_x = center.x()
        element_y = center.y()
        web_view_width = self.width()
        web_view_height = self.height()
        scroll_right = 0
        scroll_down = 0

        if element_x > web_view_width:
            scroll_right = element_x - web_view_width + element_geometry.width() / 2 + 50

        if element_y > web_view_height:
            scroll_down = element_y - web_view_height + element_geometry.height() / 2 + 50

        self.page().mainFrame().setScrollBarValue(Qt.Horizontal, 0)
        self.page().mainFrame().setScrollBarValue(Qt.Vertical, 0)

        self.page().mainFrame().setScrollBarValue(Qt.Vertical, scroll_right)
        self.page().mainFrame().setScrollBarValue(Qt.Vertical, scroll_down)

    def scroll_and_click(self, element):

        element_geometry = element.geometry()
        center = element_geometry.center()
        element_x = center.x()
        element_y = center.y()
        web_view_width = self.width()
        web_view_height = self.height()
        scroll_right = 0
        scroll_down = 0

        if element_x > web_view_width:
            scroll_right = element_x - web_view_width + element_geometry.width() / 2 + 50

        if element_y > web_view_height:
            scroll_down = element_y - web_view_height + element_geometry.height() / 2 + 50

        self.page().mainFrame().setScrollBarValue(Qt.Horizontal, 0)
        self.page().mainFrame().setScrollBarValue(Qt.Vertical, 0)

        self.page().mainFrame().setScrollBarValue(Qt.Vertical, scroll_right)
        self.page().mainFrame().setScrollBarValue(Qt.Vertical, scroll_down)
        point_to_click = QPoint(element_x - scroll_right, element_y - scroll_down)

        self.page().event(QMouseEvent(QMouseEvent.MouseButtonPress, point_to_click , Qt.LeftButton, Qt.LeftButton, Qt.NoModifier))
        self.page().event(QMouseEvent(QMouseEvent.MouseButtonRelease, point_to_click , Qt.LeftButton, Qt.LeftButton, Qt.NoModifier))

    def click_on_all(self, elementsCollection, interval=1):
        if isinstance(elementsCollection, QWebElementCollection):
            for e in elementsCollection:
                self.click(e)
                self.wait(interval)

    def attr(self, element, name, value=None):
        if isinstance(element, QWebElement):
            if value is None:
                return element.attribute(name)
            else:
                element.setAttribute(name, value)
                return True

        return False

    def attr_all(self, elements, name, value=None):

        if isinstance(elements, QWebElementCollection):
            if value is None:
                att_list = []
                for e in elements:
                    el_attr = e.attribute(name)
                    if el_attr:
                        att_list.append(e.attribute(name))
                return att_list
            else:
                for e in elements:
                    e.setAttribute(name, value)
                return True

        return False

    def fill(self, element, value):
        if isinstance(element, QWebElement):
            tag = str(element.tagName()).lower()
            if tag == 'input':
                return self.attr(element, 'value', value)
            else:
                element.setPlainText(value)
                return True
        return False

    def fill_all(self, elements, value):
        if isinstance(elements, QWebElementCollection):
            for e in elements:
                self.fill(e, value)

    def select_by_text(self, element, text):
        if not isinstance(element, QWebElement):
            raise Exception("QWebElement expected")
        element.evaluateJavaScript(
            " var elem = this ; for (var i = 0; i < elem.options.length; i++) { "
            " if (elem.options[i].text === \"" + text +
            "\") { elem.selectedIndex = i; break; } } ")
        self.js_init_event("change")

    def select_by_text_contain(self, element, text):
        if not isinstance(element, QWebElement):
            raise Exception("QWebElement expected")
        element.evaluateJavaScript(
            " var elem = this ; for (var i = 0; i < elem.options.length; i++) { "
            "if (/"+text+"/i.test(elem.options[i].text)) { elem.selectedIndex = i; break; } } ")
        self.js_init_event("change")

    def select_by_value(self, element, value):
        if not isinstance(element, QWebElement):
            raise Exception("QWebElement expected")
        element.evaluateJavaScript(
            " var elem = this ; for (var i = 0; i < elem.options.length; i++) { "
            " if (elem.options[i].value === \"" + value +
            "\") { elem.selectedIndex = i; break; } } ")
        self.js_init_event("change")

    def js_init_event(self, name):
        self.js_exec("if ('createEvent' in document) { " +
                     "var evt = document.createEvent('HTMLEvents'); " +
                     "evt.initEvent('change', false, true); " +
                     "elem.dispatchEvent(evt); }")

    def get_all_links_by_text(self, link_text):
        links = self.find_elements("a");
        links_for_click = []
        for link in links:
            if link_text.lower() in link.toPlainText().lower():
                links_for_click.append(link)
        return links_for_click

    def click_on_link_by_text(self, link_text, occurence):
        links = self.get_all_links_by_text(link_text)
        links_count = len(links)
        if links_count == 0 or links_count < occurence or occurence <= 0:
            return False
        return self.click(links[occurence - 1])

    def click_on_first_link_by_text(self, link_text):
        return self.click_on_link_by_text(link_text, 1)

    def click_on_last_link_by_text(self, link_text):
        links = self.get_all_links_by_text(link_text)
        links_count = len(links)
        if links_count == 0:
            return False
        return self.click(links[-1])

    def count_elements_with_text(self, selector, text):
        elements = self.find_elements(selector)
        count = 0
        for element in elements:
            if text in element.toPlainText():
                ++count
        return count

    def screen_shot(self, element, output_file):
        """Take screen shot of current webpage and save results
        """
        frame = self.page().mainFrame()
        self.page().setViewportSize(frame.contentsSize())
        image = QImage(self.page().viewportSize(), QImage.Format_ARGB32)
        painter = QPainter(image)
        frame.render(painter)
        painter.end()
        log.logger.debug('saving ' + output_file)
        image.save(output_file)


class WebPage(QWebPage):
    def __init__(self, user_agent, confirm=True):
        # super(WebPage, self).__init__()
        QWebPage.__init__(self)
        self.user_agent = user_agent
        self.confirm = confirm

    def userAgentForUrl(self, url):
        return self.user_agent

    def javaScriptAlert(self, frame, message):
        """Override default JavaScript alert popup and print results
        """
        log.logger.debug('Alert:' + message)

    def javaScriptConfirm(self, frame, message):
        """Override default JavaScript confirm popup and print results
        """
        log.logger.debug('Confirm:' + message)
        return self.confirm

    def javaScriptPrompt(self, frame, message, default):
        """Override default JavaScript prompt popup and print results
        """
        log.logger.debug('Prompt:%s%s' % (message, default))

    def javaScriptConsoleMessage(self, message, line_number, source_id):
        """Print JavaScript console messages
        """
        log.logger.debug('Console:%s%s%s' % (message, line_number, source_id))

    def shouldInterruptJavaScript(self):
        """Disable javascript interruption dialog box
        """
        return True


class UserAgent:
    # def firefox_browser(os_version):
    # browser_version = random.randint(30, 34)
    # return 'Mozilla/5.0 (%s; rv:%d.0) Gecko/20100101 Firefox/%d.0' % (os_version, browser_version, browser_version)
    #
    #
    # def ie_browser(os_version=None):
    # os_version = windows_os()  # always use windows with IE
    # return 'Mozilla/5.0 (compatible; MSIE %d.0; %s; Trident/%d.0)' % (
    # random.randint(8, 10), os_version, random.randint(5, 6))

    def chrome_browser(self):
        return 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36'


    def rand_agent(self):
        return self.chrome_browser()