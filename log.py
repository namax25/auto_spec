__author__ = 'namax_w7'
# -*- coding: utf-8 -*-

import os
import sys
import logging
import logging.handlers

def create_logger(log_file_name, maxbytes=0):
    logger = logging.getLogger(log_file_name)
    if not logger.handlers:
        logger.setLevel(logging.DEBUG)
        if not maxbytes:
            file_handler = logging.FileHandler(log_file_name)
        else:
            file_handler = logging.handlers.RotatingFileHandler(log_file_name, maxBytes=maxbytes)
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))
        logger.addHandler(file_handler)

        # console_handler = ConsoleHandler()
        # console_handler.setLevel(level)
        # logger.addHandler(console_handler)
    return logger

def get_log_file():
    dirname, filename = os.path.split(sys.argv[0])
    root_dir = os.path.join(dirname, '.' + filename.replace('.py', ''))

    if not os.path.exists(root_dir):
        try:
            os.mkdir(root_dir)
        except OSError as e:
            root_dir = '.log'
    return os.path.join(root_dir, 'error.log')

    logging.basicConfig(filename=log_file,level=logging.DEBUG)

logger = create_logger(get_log_file(), maxbytes=2 * 1024 * 1024 * 1024)

