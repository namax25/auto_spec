__author__ = 'namax_w7'
# -*- coding: utf-8 -*-

import sys
import os
import logging


# default location to store output state files
dirname, filename = os.path.split(sys.argv[0])
state_dir = os.path.join(dirname, '.' + filename.replace('.py', ''))
if not os.path.exists(state_dir):
    try:
        os.mkdir(state_dir)
    except OSError as e:
        state_dir = ''
        # print 'Unable to create state directory:', e

print(state_dir)

default_encoding = 'utf-8'
default_headers = {
    'Accept-Language': 'en-us,en;q=0.5',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
}
